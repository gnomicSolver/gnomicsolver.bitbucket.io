var searchData=
[
  ['operator_2a',['operator*',['../structgnomic_1_1BaseFlatState.html#a6aea2073895471b4db82ff5ce0fa181a',1,'gnomic::BaseFlatState']]],
  ['operator_2b',['operator+',['../structgnomic_1_1BaseFlatState.html#ad7589442a6cea40a51c6a39b5d284c4a',1,'gnomic::BaseFlatState']]],
  ['operator_2d',['operator-',['../structgnomic_1_1BaseFlatState.html#a832b31c0eb5e11f0a824ad73f77faabf',1,'gnomic::BaseFlatState::operator-()'],['../structgnomic_1_1BaseStateVector.html#abfc892d441ce11424beb152dc5984660',1,'gnomic::BaseStateVector::operator-()']]],
  ['operator_3d',['operator=',['../structgnomic_1_1BaseFlatState.html#ae587003ea9cdde69d54b4f3b60e41eb5',1,'gnomic::BaseFlatState']]],
  ['optimaltrajectory',['optimalTrajectory',['../classgnomic_1_1Solver_3_01Loss_3_01Input_00_01State_00_01FlatState_01_4_00_01typename_01std_1_1e39dbe7c4dd2a576a96c9f870e09a6980.html#acc256100861869a30612aba44591d4a4',1,'gnomic::Solver&lt; Loss&lt; Input, State, FlatState &gt;, typename std::enable_if&lt; std::is_base_of&lt; BaseLoss&lt; Input, State, FlatState &gt;, Loss&lt; Input, State, FlatState &gt; &gt;::value &gt;::type &gt;']]]
];
